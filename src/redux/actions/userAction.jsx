import { LOGIN } from "../contants/userContant";


export let loginAction = (loginData) => {
    return {
        type: LOGIN,
        payload: loginData
    };
};