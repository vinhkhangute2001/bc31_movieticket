import { SET_MOVIE_LIST } from "../contants/movieContant";

const initialState = {
    movieList: []
};

export let movieReducer = (state = initialState, { type, payload }) => {
    switch (type) {

        case SET_MOVIE_LIST:
            state.movieList = payload;
            return { ...state };

        default:
            return state;
    }
};

