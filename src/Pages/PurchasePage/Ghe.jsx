import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { addSeatToCart } from "../../redux/actions/seatAction";

export default function Ghe({ ghe }) {
  const [tinhTrangGhe, setTinhTrangGhe] = useState("available");
  let dispatch = useDispatch();

  useEffect(() => {
    if (ghe.daDat) {
      setTinhTrangGhe("unavailable");
    } else if (ghe.loaiGhe === "Vip") {
      setTinhTrangGhe("vip");
    }
  }, []);

  let handleSetStatus = (daDat, loaiGhe) => {
    daDat
      ? loaiGhe === "Vip"
        ? setTinhTrangGhe("vip")
        : setTinhTrangGhe("available")
      : setTinhTrangGhe("selected");
  };

  return (
    <div className={`seatCharts-seat seatCharts-cell ` + tinhTrangGhe}>
      <label htmlFor={`btn-${ghe.stt}`}>{ghe.tenGhe}</label>
      <button
        type="button"
        id={`btn-${ghe.stt}`}
        disabled={tinhTrangGhe === "unavailable"}
        onClick={() => {
          handleSetStatus(ghe.daDat, ghe.loaiGhe);
          dispatch(addSeatToCart(ghe, ghe.maGhe));
        }}
      ></button>
    </div>
  );
}
