import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import getMovieListActionService from "../../redux/actions/movieAction";
import "./listMovie.css";

export default function ListMovie({ listMovieRef }) {
  const [isOpen, setOpen] = useState(true);
  let dispatch = useDispatch();
  let { movieList } = useSelector((state) => {
    return state.movieReducer;
  });

  useEffect(() => {
    dispatch(getMovieListActionService());
  }, []);

  let renderMovieList = () => {
    return movieList.map((movie) => {
      return (
        <div
          key={movie.maPhim}
          className="bg-[#2b2c33] grid md:grid-cols-1 p-2 mx-1 rounded-md"
        >

          <div className="hover:shadow-md hover:shadow-slate-100 hover:scale-110 hover:duration-500 h-full flex items-center justify-center rounded-md">
            <img
              className="rounded-md object-cover top-0  w-full  h-52  md:h-80"
              src={movie.hinhAnh}
              alt="..."
            />
          </div>
          <div className="flex pt-2 h-16">
            <p className="space-x-4  py-1 px-2">
              <span className="px-2 py-1 bg-yellow-400 text-black  rounded-md font-bold">
                {movie.maNhom}
              </span>
              <span className="text-slate-500">|</span>
              <span className="text-center text-white text-sm md:text-lg font-bold">
                {movie.tenPhim}
              </span>
            </p>
          </div>
          <NavLink to={`detail/${movie.maPhim}`}>
            <div className=" mt-2">
              <button className="rounded w-full md:rounded-lg bg-red-700 py-1 px-2 md:px-5 md:py-2.5 text-center text-sm font-medium text-white hover:bg-red-800 focus:outline-none focus:ring-4 focus:ring-red-300 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800">
                Mua vé
              </button>
            </div>
          </NavLink>

        </div>
      );
    });
  };

  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 5,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 5,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 3,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 2,
    },
  };

  let showAllMovie = () => {
    setOpen(false);
  };

  return (
    <div className="">
      <div className="bg-[#202020]">
        <div
          div
          className="h-12 sm:h-20 px-2 sm:px-4 lg:px-4   max-w-7xl mx-auto flex justify-between items-center"
        >
          <p
            className="text-white md:text-xl font-extrabold uppercase"
            id="danhSachPhim"
          >
            Danh sách phim
          </p>
          {isOpen ? (
            <button
              className="text-red-500 md:bg-[rgba(24,36,70,0.5)] md:hover:bg-[#3533457d] hover:scale-125 font-medium  hover:text-red-500 hover:duration-200 py-2 px-3 rounded-md"
              onClick={showAllMovie}
            >
              Xem tất cả {">"}
            </button>
          ) : (
            <button
              className="text-red-500 md:bg-[rgba(24,36,70,0.5)] md:hover:bg-[#3533457d] hover:scale-125  font-medium  hover:text-red-700 hover:duration-200 py-2 px-3 rounded-md"
              onClick={() => {
                setOpen(true);
              }}
            >
              {"<"} Trở về
            </button>
          )}
        </div>
      </div>
      <div className="mt-2 sm:mt-4 px-2 sm:px-4 bg-black">
        {isOpen ? (
          <Carousel
            swipeable={true}
            draggable={true}
            // showDots={true}
            responsive={responsive}
            ssr={true} // means to render carousel on server-side.
            infinite={true}
            autoPlay={true}
            // autoPlaySpeed={2000}
            keyBoardControl={true}
            customTransition="all .5"
            // transitionDuration={100}
            containerClass="carousel-container"
            // removeArrowOnDeviceType={["tablet",]}
            // deviceType={this.props.deviceType}
            dotListClass="custom-dot-list-style"
            className="max-w-7xl mx-auto"
          >
            {renderMovieList()}
          </Carousel>
        ) : (
          <div className="max-w-7xl mx-auto">
            <div className="grid grid-cols-2 lg:grid-cols-5 sm:grid-cols-3  gap-y-5 items-start  ">
              {renderMovieList()}
            </div>
          </div>
        )}
      </div>
    </div>
  );
}
