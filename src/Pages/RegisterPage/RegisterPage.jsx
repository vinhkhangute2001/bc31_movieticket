import React from "react";
import logoCineMax from "../../assets/Logo-cinemax.png";
import { LockClosedIcon } from "@heroicons/react/24/outline";
import { Checkbox, Form, Input, message, Select } from "antd";
import { Link, useNavigate } from "react-router-dom";
import { userService } from "../../services/userService";

const { Option } = Select;

export default function RegisterPage() {
  let navigate = useNavigate();

  const [form] = Form.useForm();

  const onFinish = (values) => {
    console.log("Received values of form: ", values);
    userService
      .postRegister(values)
      .then((res) => {
        console.log(res);
        if (res.data) {
          message.success("Đăng ký thành công");
          setTimeout(() => {
            navigate("/login");
          }, 3000);
          return res.data;
        }
      })
      .catch((err) => {
        console.log(err);
        return err.response.data.content;
      });
  };

  // const prefixSelector = (
  //   <Form.Item name="prefix" noStyle >
  //     <Select
  //       style={{
  //         width: 70,

  //       }}
  //       className="rounded-md"
  //     >
  //       <Option value="84">+84</Option>
  //     </Select>
  //   </Form.Item>
  // );

  return (
    <div className="flex min-h-screen items-center justify-center p-6">
      <div className="bg-login w-full min-h-screen absolute"></div>
      <div className="bg-[#000000] relative z-10 w-full max-w-md space-y-8 rounded-lg p-6 ">
        <div className="flex flex-col items-center ">
          <img className="mx-auto h-12 w-auto" src={logoCineMax} alt="" />
          <p className="text-white text-center text-4xl font-bold my-2">Đăng ký </p>
          <p className="mt-2 text-center text-sm text-gray-600">
            Hoặc &nbsp;
            <Link
              to="/login"
              className="font-medium text-indigo-600 hover:text-indigo-500"
            >
              Đăng nhập
            </Link>
          </p>
        </div>
        <div className="">
          <Form
            layout="vertical"
            // {...formItemLayout}
            form={form}
            name="register"
            onFinish={onFinish}
            initialValues={{
              prefix: "84",
            }}
            scrollToFirstError
          >
            {/* Tài khoản */}
            <Form.Item
              type="string"
              name="taiKhoan"
              tooltip="What do you want others to call you?"
              rules={[
                {
                  required: true,
                  message: "vui lòng nhập tài khoản!",
                  whitespace: true,
                },
              ]}
            >
              <Input placeholder="Tài khoản" className="rounded-md" />
            </Form.Item>

            {/* Mật khẩu */}
            <Form.Item
              type="string"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "vui lòng nhập mật khẩu!",
                },
              ]}
              hasFeedback
            >
              <Input.Password
                placeholder="Mật khẩu"
                className="rounded-md h-10"
              />
            </Form.Item>

            {/* Email */}
            <Form.Item
              type="string"
              name="email"
              rules={[
                {
                  type: "email",
                  message: "The input is not valid E-mail!",
                },
                {
                  required: true,
                  message: "vui lòng nhập E-mail!",
                },
              ]}
            >
              <Input placeholder="Email" className="rounded-md" />
            </Form.Item>

            {/* Số-ĐT */}
            <Form.Item
              type="string"
              name="soDT"
              rules={[
                {
                  required: true,
                  message: "vui lòng nhập số điện thoại!",
                },
              ]}
            >
              <Input
                placeholder="Số ĐT"
                // addonBefore={prefixSelector}
                className="rounded-md"
              />
            </Form.Item>

            {/* Mã nhóm */}
            {/* <Form.Item
                            type="string"
                            name="maNhom"
                            tooltip="What do you want others to call you?"
                            rules={[
                                {
                                    required: true,
                                    message: 'vui lòng nhập mã nhóm!',
                                    whitespace: true,
                                },
                            ]}
                        >
                            <Input placeholder='Mã nhóm' className='rounded-md' />
                        </Form.Item> */}

            {/* Họ-Tên */}
            <Form.Item
              type="string"
              name="hoTen"
              tooltip="What do you want others to call you?"
              rules={[
                {
                  required: true,
                  message: "vui lòng nhập họ tên!",
                  whitespace: true,
                },
              ]}
            >
              <Input placeholder="Họ tên" className="rounded-md" />
            </Form.Item>

            <Form.Item
              name="agreement"
              valuePropName="checked"
              rules={[
                {
                  validator: (_, value) =>
                    value
                      ? Promise.resolve()
                      : Promise.reject(new Error("Đồng ý với thỏa thuận")),
                },
              ]}
            >
              <Checkbox className="text-white">
                Tôi đồng ý với  thỏa thuận <a href="">cinemax</a>
              </Checkbox>
            </Form.Item>

            <button
              type="submit"
              className="group relative flex w-full justify-center rounded-md border border-transparent bg-[#e5b60d] py-2 px-4 text-sm font-medium text-black hover:text-[#e5b60d] hover:bg-gray-700 hover:duration-500  focus:outline-none focus:ring-2 focus:ring-yellow-400 focus:ring-offset-2"
            >
              <span className="absolute inset-y-0 left-0 flex items-center pl-3">
                <LockClosedIcon
                  className="h-5 w-5 text-white group-hover:text-[#FF5505]"
                  aria-hidden="true"
                />
              </span>
              Tạo tài khoản
            </button>
          </Form>
        </div>
      </div>
    </div>
  );
}
