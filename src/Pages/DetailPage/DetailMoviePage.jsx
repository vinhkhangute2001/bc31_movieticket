import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { movieService } from "../../services/movieService";
import { DownOutlined } from '@ant-design/icons';
import { Breadcrumb } from "flowbite-react";
import { HomeIcon } from "@heroicons/react/20/solid";
import TabMovieDetail from "./TabMovieDetail";
import MovieDetail from "./MovieDetail";

export default function DetailMoviePage() {
  let param = useParams();
  const [movieDetail, setMovieDetail] = useState({});

  useEffect(() => {
    setTimeout(() => {
      movieService
        .getDetailMovie(param.id)
        .then((res) => {
          setMovieDetail(res.data.content);
        })
        .catch((err) => {
          console.log("err", err);
        });
    }, 500);
  }, []);

  return (
    <div>



      <div id="movieDetail " className="pt-10 bg-black">
        <div className="bg-[#2b2c33] h-12 max-w-7xl mx-auto rounded mt-20">

          <Breadcrumb
            className=" py-3 px-5 "
          >
            <Breadcrumb.Item
              href="/"
            >
              <span className="text-gray-400 flex items-center hover:underline"><HomeIcon className="h-6 w-6 mr-2 " />Trang chủ</span>
            </Breadcrumb.Item>
            <Breadcrumb.Item href="#">
              <span className="text-white flex items-center hover:underline">Chi tiết phim</span>
            </Breadcrumb.Item>
          </Breadcrumb>

        </div>
        <div className="max-w-7xl mx-auto ">
          <MovieDetail movieDetail={movieDetail} />

          <div className="flex flex-col mt-10 items-center" >
            <p className="text-white font-bold text-2xl px-3 py-2 rounded-sm bg-red-700 mb-8">Mua vé</p>
            <p className="w-8 h-8 text-center mx-auto animate-bounce " ><DownOutlined style={{ fontSize: '26px', color: 'red', fontWeight: "bold" }} /></p>
          </div>
          <TabMovieDetail maPhim={param.id} />
        </div>
      </div>
    </div>
  );
}
