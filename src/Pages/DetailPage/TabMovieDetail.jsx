import React, { useEffect, useState } from "react";
import { Tabs } from "antd";
import { movieService } from "../../services/movieService";
import ItemTabMovieDetail from "./ItemTabMovieDetail";
import TabPane from "antd/lib/tabs/TabPane";
export default function TabMovieDetail({ maPhim }) {
  const [dataRaw, setDataRaw] = useState([]);

  useEffect(() => {
    movieService
      .getDetailTabMovie(maPhim)
      .then((res) => {
        setDataRaw(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const renderContent = () => {
    return dataRaw.heThongRapChieu?.map((cumRap, index) => {
      return (
        <TabPane
          className="shadow-sm-light bg-[#202020]"
          tab={
            <div className="border-2 rounded-full ">
              <img src={cumRap.logo} alt="" width="45px" />
            </div>
          }
          key={index}
        >
          <Tabs
            style={{ height: 500, color: "white" }}
            tabPosition="left"
            defaultActiveKey="1"
            className=" bg-[#202020]  shadow-sm-light"
          >
            {cumRap.cumRapChieu?.map((rapChieu, index) => {
              return (
                <TabPane
                  tab={
                    <div className="hover:bg-slate-800">
                      {renderTenCumRap(rapChieu)}
                    </div>
                  }
                  key={rapChieu.maCumRap}
                >
                  <div
                    style={{ height: 500, overflowY: "scroll" }}
                    className="shadow-sm-light"
                  >
                    <ItemTabMovieDetail rapChieu={rapChieu} key={index} />
                  </div>
                </TabPane>
              );
            })}
          </Tabs>
        </TabPane>
      );
    });
  };

  const renderTenCumRap = (cumRapChieu) => {
    return (
      <div className="text-left w-60 whitespace-normal">
        <p className="text-[#24a3ac] truncate active:text-[#00ffea]">
          {cumRapChieu.tenCumRap}
        </p>
        <p className="text-white truncate">{cumRapChieu.diaChi}</p>
        <button className="text-red-500">[ Xem chi tiết ]</button>
      </div>
    );
  };
  return (
    <div className="max-w-7xl mx-auto my-12">
      <div className="bg-[#202020]">
        <div
          div
          className="h-12 sm:h-20 px-2 sm:px-4 lg:px-4  max-w-7xl mx-auto flex justify-between items-center"
        >
          <p className="text-white md:text-xl font-extrabold uppercase">
            Cụm rạp
          </p>
          <p className="text-red-700 md:text-xl font-extrabold uppercase">
            Lịch chiếu
          </p>
        </div>
      </div>

      <Tabs
        tabPosition="left"
        defaultActiveKey="1"
        className="scroll bg-[#202020] shadow-sm-light rounded-lg py-5 mt-5 "
      >
        {renderContent()}
      </Tabs>
    </div>
  );
}
