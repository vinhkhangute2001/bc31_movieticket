import moment from "moment";
import React from "react";
import { NavLink } from "react-router-dom";

export default function ItemTabMovieDetail({ rapChieu }) {
  return (
    <div className="bg-[#202020] my-1 p-2 rounded-md">
      <div className="grid grid-cols-4 gap-4">
        {rapChieu.lichChieuPhim.slice(0, 8).map((lichChieu, index) => {
          return (
            <NavLink to={`/purchase/${lichChieu.maLichChieu}`} key={index}>
              <div className="bg-red-800 text-white p-1 rounded">
                {moment(lichChieu.ngayChieuGioChieu).format("DD/MM/yyyy")}
                <span className="text-yellow-300 font-bold text-base ml-7">
                  {moment(lichChieu.ngayChieuGioChieu).format("hh:mm")}
                </span>
              </div>
            </NavLink>
          );
        })}
      </div>
    </div>
  );
}
