import axios from "axios";
import { https, TOKEN_CYBER } from "./configURL";
import { localStorageService } from "./localStorageService";

export let movieService = {
  getBannerMovie: () => {
    return https.get("api/QuanLyPhim/LayDanhSachBanner");
  },
  getMovieList: () => {
    return https.get("/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP10");
  },
  getMovieDetail: (id) => {
    return https.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`);
  },
  getMovieByTheater: () => {
    return https.get(
      "/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP10"
    );
  },
  // DetailPage
  getDetailMovie: (param) => {
    return https.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${param}`);
  },
  getDetailTabMovie: (param) => {
    return https.get(`/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${param}`);
  },
  // PurchasePage
  getSeatByMovie: (param) => {
    return https.get(
      `/api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${param}`
    );
  },
  postPurchase: (loginData) => {
    return https.post("/api/QuanLyNguoiDung/DangNhap", loginData);
  },
  // Booking Ticket
  postBookingTicket: (values) => {
    return axios({
      url: "https://movienew.cybersoft.edu.vn/api/QuanLyDatVe/DatVe",
      method: "POST",
      data: values,
      headers: {
        TokenCybersoft: TOKEN_CYBER,
        ["Authorization"]:
          "Bearer " + localStorageService.user.get().accessToken,
      },
    });
  },
};
