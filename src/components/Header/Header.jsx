import React from "react";
import logoCineMAx from "../../assets/Logo-cinemax.png";
import UserNav from "../UserNav/UserNav";
import { Disclosure } from "@headlessui/react";
import { Bars3Icon, UserGroupIcon, UserIcon, XMarkIcon } from "@heroicons/react/24/outline";
import { NavLink } from "react-router-dom";
import { useSelector } from "react-redux";
import { UserCircleIcon } from "@heroicons/react/20/solid";

const navigation = [
  { name: "Danh sách phim", ref: "/#danhSachPhim", current: false },
  { name: "Cụm rạp", ref: "/#cumRap", current: false },
  { name: "Tin tức", ref: "/#tinTuc", current: false },
  { name: "Ứng dụng", ref: "/#ungDung", current: false },
];

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

export default function Header() {
  let userInfor = useSelector((state) => {
    return state.userReducer.userInfor;
  });
  return (
    <div>
      <Disclosure
        Disclosure
        as="nav"
        className="bg-[#202020] fixed bg-fixed z-10 max-w-7xl min-w-full shadow-rose-600 shadow-md  "
      >
        {({ open }) => (
          <>
            <div className="mx-auto max-w-7xl px-2 sm:px-6 lg:px-8">
              <div className="relative flex h-[90px] items-center justify-between">
                <div className="absolute inset-y-0 left-0 flex items-center sm:hidden">
                  {/* Mobile menu button*/}
                  <Disclosure.Button className="inline-flex items-center justify-center rounded-md p-2 text-gray-400 hover:bg-gray-700 hover:text-white focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white">
                    <span className="sr-only">Open main menu</span>
                    {open ? (
                      <XMarkIcon className="block h-6 w-6" aria-hidden="true" />
                    ) : (
                      <Bars3Icon className="block h-6 w-6" aria-hidden="true" />
                    )}
                  </Disclosure.Button>
                </div>
                <div className="flex flex-1 items-center justify-center sm:items-stretch sm:justify-start">
                  <div className="flex flex-shrink-0 items-center">
                    <NavLink to={"/"}>
                      <img
                        className="h-12 w-auto"
                        src={logoCineMAx}
                        alt="logoCineMAx"
                      />
                    </NavLink>
                  </div>
                  <div className="hidden pt-[4px] sm:ml-6 sm:block ">
                    <div className="flex space-x-4">
                      <input
                        type="text"
                        className="rounded-full bg-[#2b2c33] h-8 hidden w-60 lg:block px-5 font-medium focus:outline-none focus:shadow-outline focus:border-red-300"
                        placeholder="Tìm kiếm"
                      />
                      {navigation.map((item) => (
                        <a
                          href={item.ref}
                          key={item.name}
                          className={classNames(
                            item.current
                              ? "bg-gray-800 text-[#36ffe8]"
                              : "text-gray-300 hover:bg-gray-800 hover:text-[#33ff18]",
                            "px-3 py-2 rounded-md text-sm font-medium"
                          )}
                          aria-current={item.current ? "page" : undefined}
                        >
                          {item.name}
                        </a>
                      ))}
                    </div>
                  </div>
                </div>
                <UserNav />
              </div>
            </div>
            <Disclosure.Panel className="sm:hidden">
              <div className="space-y-1 px-2 pt-2 pb-3">
                {navigation.map((item) => (
                  <div>
                    <Disclosure.Button
                      key={item.name}
                      as="a"
                      href={item.ref}
                      className={classNames(
                        item.current
                          ? "bg-gray-900 text-white"
                          : "text-gray-300 hover:bg-gray-800 hover:text-white",
                        "block px-3 py-2 rounded-md text-base font-medium"
                      )}
                      aria-current={item.current ? "page" : undefined}
                    >
                      {item.name}

                    </Disclosure.Button>

                  </div>
                ))}
                {!userInfor && <div className="grid grid-cols-2 gap-x-3  pt-2 pb-3  rounded-md text-base font-medium">
                  <NavLink to="/login">
                    <p className="flex items-center justify-center bg-slate-800 text-[#FF0505]  hover:bg-[#FF0505] hover:text-white rounded-md px-2 py-2">
                      <span><UserIcon className="w-6 h-6 text-white" /></span>
                      <span className="px-2">Đăng nhập</span>
                    </p>
                  </NavLink>
                  <NavLink to="/register">
                    <p className=" flex items-center justify-center bg-slate-800 text-[#e5b60d]  hover:bg-[#e5b60d] hover:text-black rounded-md px-2 py-2">
                      <span ><UserGroupIcon className="w-6 h-6 text-white" /></span>
                      <span className="px-2">Đăng ký</span>
                    </p>
                  </NavLink>
                </div>}

              </div>
            </Disclosure.Panel>
          </>
        )
        }
      </Disclosure >
    </div >
  );
}
