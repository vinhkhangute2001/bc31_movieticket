import React from "react";
import Footer from "../../components/Footer/Footer";
import Header from "../../components/Header/Header";

export default function Layout({ Component }) {
  return (
    <div className="bg-black">
      <Header />
      <Component />
      <Footer />
    </div>
  );
}
